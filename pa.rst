=======================
Programmazione avanzata
=======================

Per accedere alla slide si usano:

Username
  ``pa``
Password
  ``PA+#2009#``

Introduzione a Python
=====================

Python è un *general purpose high-level programming language*.

Supporta vari paradigmi di programmazione:

- imperativo
- ad oggetti
- funzionale

Tipi primitivi
--------------

In Python i tipi formalmente non esistono, anche se ogni datatype ha un valore
associato in memoria, che è gestito internamente.

Le variabili non sono contenitori, ma label a zone di memoria.

Operatore ``type()``
>>>>>>>>>>>>>>>>>>>>

Qualsiasi cosa in Python è un *first class object*, quindi ha una classe.
(Anche il tipo è di tipo ``type``).

L'operatore ``type(var)`` risponde con il tipo della variabile data come
argomento ed è built-in nel linguaggio.
Esiste anche ``isinstance(var, class)`` che è il formato a predicato di
``type(var)`` e risponde appunto con un booleano.

Liste
>>>>>

``list`` in python è sia il tipo lista, sia il costruttore di una lista vuota.
Le liste sono implementate come liste linkate, anche se si possono vedere come
degli array di grandezza variabile.

Notevoli sono:

- l'eterogeneità delle liste

  >>> a = [1, '1', True, 2.0]

- la possibilità di usare indici negativi
  
  >>> 'ciao'[-1]
  'o'

- l'operatore di slicing

  >>> 'ciao'[1:3]
  'ia'

- lo shift automatico in caso di rimozione di un lemento di una lista

Metodi di utilità per le liste sono:

- ``append()``: aggiunge un elemento in coda alla lista
- ``extend()``: appende una lista in fondo ad un'altra lista
- ``insert()``: inserisce un elemento ad una posizione specifica

Tuple
>>>>>

>>> b = (1, '1')

In linea di massima sono delle liste immutabili. Per questa proprietà sono
utilizzabili come chiavi di un dizionario.

Sono utili anche nell'assegnamento multiplo:

>>> a, b = 1, 2

Insiemi
>>>>>>>

Un set è definito come un *unordered univoque list*.

>>> c = {1, '1'}
>>> c = set()

Dizionario
>>>>>>>>>>

Equivalente di una mappa in Java. Ci vuole un oggetto immutabile come chiave e
un qualsiasi altro tipo di oggetto come valore.
Una chiave esiste solo se nel momento in cui gli viene associato un valore.

>>> d = {1: 'ciao', 2: 'hello'}
>>> d = {}

String
>>>>>>

Si comporta come una lista di caratteri.

>>> e = 'ciao'
>>> f = "ciao"
>>> g = '''ciao
... ciao'''}

``format()`` è un metodo che permette di creare una stringa che si comporta
come la ``printf()`` del C.

>>> '{0:.2f}: {1:3d}'.format(2.0, 4)

Ricorsione
----------

``n! = 1 if n=0; n(n+1) altrimenti``

La ricorsione è composta da due passi:

- passo base ``->`` n=0
- passo induttivo ``->`` n(n+1)

In Python:

.. code-block:: python3

  def fact(n): return 1 if n<=1 else n*fact(n-1)

Stack
-----

Ogni programma ha un suo stack associato. Ogni chiamata a funzione aggiunge un
**frame** allo stack.

Una funzione ricorsiva aggiunge un frame ad ogni passo della ricorsione (a meno
di una ricorsività di coda, in cui il codice non cambia, ma che in Python non è
supportata). In genere la ricorsione è più lenta dell'iterazione a causa della
creazione di questi frame.

Nello *stack trace*, anche detto *record di attivazione*, sono contenuti tutti
quei valori fondamentali della funzione, quali parametri, valore di ritorno,
program counter, puntatore allo heapm, etc).

Esempio
-------

.. code-block:: python3
  
  import os, sys, time, humanize
  from stat import *

  modes = {
    'r': (S_IRUSR, S_IRGRP, S_IROTH),
    'w': (S_IWUSR, S_IWGRP, S_IWOTH),
    'x': (S_IXUSR, S_IXGRP, S_IXOTH)
  }

  def format_mode(mode):
    s = 'd' if S_ISDIR(mode) elsee "-"
    for i in range(3):
      for j in ['r', 'w', 'x']:
        s += j if S_IMODE(mode) & modes[j][i] else '-'
    return s

  def format_date(date):
    d = time.localtime(date)
    return "{0:4}-{1:02d}-{2:02d} {3:02d}:{4:02d}:{5:02d}".format(d.tm_year,
      d.tm_mon, d.tm_mday, d.tm_hour, d.tm_min, d.tm_sec)

  def ls(dir):
    print("List of {0}:".format(dir))
    for file in os.listdir(dir):
      metadata = os.stat(file)
      print("{2} {1:6} {3} {0} ".format(file, approximate_size(metadata.st_size,
        False), format_mode(metadata.st_mode), format_date(metadata.st_mtime)))

  if __name__ == '__main__': ls(sys.argv[1])

List comprehension
------------------

Descrizione estensionale di un insieme
  Al posto che descrivere elemento per elemento un insieme, ne si descrive la
  proprietà che caratterizza tutti gli elementi dell'insieme

La list comprehension è la maniera di Python di permettere una descrizione di
un insieme di questo tipo.

>>> [elem for elem in range(10)]
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

>>> {elem*2 for elem in range(10)}
{2, 4, 6, 8, 10, 12, 14, 16, 18}

>>> {elem:elem**2 for elem in range(10)}
...

I quadrati perfetti minori di 100:

>>> [elem for elem in range(100) if (int(elem**.5))**2 == elem]
...

Solo i numeri dispari:

>>> {x for x in (1, 22, 31, 23, 10, 11, 11, -1, 34, 76, 778, 10101, 5, 44) if
x%2 != 0}
{1, 5, 10101, 31, 23, 11, -1}

Inversione della coppia chiave, valore in valore, chiave

>>> a_dict = {'a': 1, 'b': 2, 'c': 3}
>>> {value:key for key, value in a_dict.items()}
{'1': a, '2': b, '3': c}}

Prodotto cartesiano

>>> {(x, y) for x in range(3) for y in range(5)}

Esempio: crivello di Eratostene
-------------------------------

Iterativamente:

.. code-block:: python3

  def is_prime(x):
    div = 2
    while div <= math.sqrt(x):
      if x % dive == 0: return False
      else: div += 1
    return True

  if __name__ == '__main__':
    primes = []
    for i in range(1, 50):
      if is_prime(i): primes.append(i)
    print(primes)

Con le list comprehension:

.. code-block:: python3

  def is_prime(x):
    div = [elem for elem in range(2,int(math.sqrt(x))+1) if x%elem==0]
    return len(div) == 0

  if __name__ == '__main__':
    print([elem for elem in range(1, 50) if is_prime(elem)])

Esempio: quicksort
------------------

.. code-block:: python3

  def quicksort(s):
    if len(s) == 0: return []
    else:
      return quicksort([x for x in s[1:] if x<s[0]]) + [s[0]] +
        quicksort([x for x in s[1:] if x >= s[0]])

  if __name__ == '__main__':
    print(quicksort('pineapple'))
    print(''.join(quicksort('pineapple')))

Programmazione funzionale
=========================

Il paradigma che abbiamo visto finora è quello ad oggetti; con il paradigma
funzionale si cambia il meccanismo di progettazione dell'applicazione, tramite
l'uso di funzioni.

- le funzioni sono **oggetti di prima classe** e come tali possono essere assegnate
  a variabili e trattate esattamente come numeri
- l'unico modo di ciclare è attraverso la **ricorsione**
- si organizzano i dati in **liste**
- evitare i **side-effects**, cioè non abbiamo variabili

Vantaggi
--------

I principali vantaggi sono la rapidità di sviluppo, la brevità e la scarsa
attinenza ad errori del codice.

In Python
---------

``map()``

  >>> import math, functools
  >>> print(list(map(math.sqrt, [x**2 for x in range(1,11)])))
  [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0]

``filter()``

  >>> def odd(x): return (x%2 != 0)
  >>> print(list(filter(odd, range(1,30))))
  [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29]

``reduce()``

  >>> def sum(x,y): return x+y
  >>> print(functools.reduce(sum, range(1000)))
  499500

Cerchiamo di eliminare anche l'``if``, tramite uno *short-circuit*.

.. code-block:: python3

  def cond(x):
    return (x==1 and 'one') or (x==2 and 'two') or 'other'
  
  if __name__ == '__main__':
    for i in range(3):
      print('cond({0}) :- {1}'.format(i, cond(i)))

Lambda
>>>>>>

La *lambda* è una funzione anonima (dal lambda-calculus).

>>> lambda e: e+1

Si può migliorare l'``if`` usando le lambda:

.. code-block:: python3

  block = lambda s: s

  cond = \
    lambda x: \
      (x==1 and block('one')) or (x==2 and block('two')) or (block('other'))

  if __name__ == '__main__':
    for i in range(3):
      print('cond({0}) :- {1}'.format(i, cond(i)))

Esempio: fattoriale
>>>>>>>>>>>>>>>>>>>

Tradizionale

.. code-block:: python3

  def fact(n):
    return 1 if n<=1 else n*fact(n-1)

Short-circuit

.. code-block:: python3

  def ffact(n):
    return (n<=1 and 1) or (n*ffact(n-1)

``map()`` e ``reduce()``

.. code-block:: python3

  from functools import reduce

  def f2fact(p):
    return reduce(lambda n,m: n*m, range(1, p+1))

Logica generale
>>>>>>>>>>>>>>>

.. code-block:: python3

  # creiamo la funzione di utilità
  do_it = lambda f: f()

  # applichiamola agli elementi
  map(do_it, [f1,f2,f3])


While
>>>>>

Statement based echo

.. code-block:: python3

  def echo_IMP():
    while True:
      x = input("FP -- ")
      if x == 'quit': break
      else: print(x)
  if __name__ = '__main__': echo_IMP()

Functional based echo

.. code-block:: python3

  # print and return to inject print into code
  def monadic_print(x):
    print(x)
    return x

  echo_FP = \
    lambda: monadic_print(input('FP -- '))=='quit' or echo_FP()
  if __name__ == '__main__': echo_FP()

``map()``-``filter()``-``reduce()``
-----------------------------------

Il futuro di questo paradigma in Python3 è incerto. ``map()`` e ``filter()``
sono banalmente delle list comprehension, mentre per ``reduce()`` si usano
delle funzioni preconfezionate in casi di più ampio uso.

Closure
=======

Espressioni regolari
--------------------

Possibilità di scrivere un pattern che rappresenta una stringa, con la
possibiltà di avere un matching.

Possibili caratteri:

- ``.`` qualsiasi carattere eccetto newline
- ``^`` inizio di stringa
- ``$`` fine di stringa

.. TODO

>>> import re
>>> re.search("^ciao$", s)

Esempio: formare i plurali di parole inglesi
--------------------------------------------

Usiamo un pattern per cercare qual è la stringa e a seconda di quale troviamo
cercheremo e sostituiremo la lettera finale.

.. code-block:: python3

  if re.search('...', s):
    re.sub('$', '...', s)
  elif re.search('...', s):
    re.sub('$', '...', s)
  elif ...

Questo if però ripete sempre lo stesso schema. Possiamo semplificare
l'algoritmo scrivendo ogni ``search`` e ``sub`` in una funzione e semplificando
in questo modo l'``if``.

Devo comunque scrivere due funzioni per ogni caso. Posso invece scrivere una
funzione sola e salvarmi i diversi parametri in una lista di tuple. Poi scrivo
una funzione che accoppia la funzione **template** con i parametri, quindi
**chiude** il template con i parametri nella lista di tuple.

.. code-block:: python3

  import re

  def build_functions(patter, search, replace):
    def matches_rule(word):
      return re.search(pattern, word)
    apply_rule = lambda word: re.sub(search, replace, word)
    return (matches_rule, apply_rule)

  patterns = [
    ('[sxz]$', '$', 'es'),
    ('...', '$', 'es'),
    ...
  ]

  rules = [
    build_functions(pattern, search, replace) \
        for (pattern, search, replace) in patterns
  ]

Questo è il concetto di **closure**.

Posso ancora dividere l'algoritmo dalle regole, salvando gli argomenti della
``build_function`` in un file e leggendoli in caso di necessità dallo stesso.
Leggere da un file è molto lento, quindi se avessi molte regole, potrebbe
rallentare l'applicazione.

Generatori
==========

I generatori permettono di calcolare un valore solo quando gli viene chiesto.

Esempio: contatore
------------------

.. code-block:: python3

  def make_counter(x):
    print('entering make_counter')
    while True:
      yield x
      print('incrementing x')
      x += 1

>>> import counter
>>> count = counter.make_counter(2)
>>> next(counter)
entering make_counter
2
>>> next(counter)
incrementing x
3

Esempio: sequenza di Fibonacci
------------------------------

.. code-block:: python3

  def gfib(max):
    a, b = 0, 1:
    while a < max:
      yield a
      a, b = b, a + b

  if __name__ == '__main__':
    for n in gfib(1000):
      print(n, end=' ')
    print()

>>> import gfib
>>> list(gfib.gfib(1000))
[0, 1, 1, 2, 3, 5, ...]

Esempio: plurali inglesi coi generatori
---------------------------------------

.. code-block:: python3

  def rules(rules_filename):
    with open(rules_filename, 'r', encoding='utf-8') as f:
      for line in f:
        pattern, search, replace = line.split(None, 3)
        yield build_functions(pattern, search, replace)

  def plural(noun, rules_filename='plural-rules.txt'):
    for matches_rule, apply_rule in rules(rules_filename):
      if matches_rules(noun):
        return apply_rule(noun)
    raise ValueError('no matching rule for {}'.format(noun))
 
Dynamic typing
==============

Python è tipizzato dinamicamente, con la differenza che l'interprete non fa
**alcuna inferenza** sul tipo delle variabili.

>>> a = 42

Il tipo è restituito sulla base dell'oggetto, infatti la variabile è solo
un'**etichetta**.
L'assegnamento nasconde tre operazioni:

- il lato destro viene creato dall'interprete sotto forma di oggetto, e.g.
  l'oggetto ``42``
- il lato sinistro crea una label nella tabella di look-up delle variabili
- viene creato un puntatore dalla label all'oggetto creato

>>> a = 42
>>> a = 'hello'
>>> a = 3.5
>>> a = [1, 2, 3]

Il nome viene riutilizzato e riassegnato nella tavola al nuovo oggetto creato.
Cosa succede al vecchio oggetto non referenziato? Viene eliminato dal *garbage 
collector* per non intasare l'heap.

>>> a = 42
>>> b = a
>>> a = 'spam'
>>> b
42

Ovviamente ``b`` punta all'oggetto ``42``, ma se cambio ``a``, ``b`` non cambia 
puntatore.

>>> a = [1, 2, 3]
>>> b = a
>>> b[1] = 'spam'
>>> b
[1, 'spam', 3]
>>> a
[1, 'spam', 3]

>>> l = [1, 2, 3]
>>> m = [1, 2, 3]
>>> n = l
>>> l == m, l is m
(True, False)
>>> l = n, l is n
(True, True)

.. warning:: Occhio

  >>> x = 42
  >>> y = 42
  >>> x == y, x is y
  (True, True)
  
  Python instanzia a runtime i primi 256 numeri per velocizzare e per favorire
  il senso comune che un numero sia uguale all'altro. Se si instanzia lo stesso 
  intero fino a 256, Python ritorna sempre lo stesso oggetto.

Gli argomenti sono passati per **valore**. L'unica eccezione sono le colleizioni
come le liste o i dizionari, ma non le tuple (immutabili per definizione), che
sono passate per **riferimento**.

Se dovessi modificare delle variabili globali all'interno ad esempio di una
funzione, dovrei usare la keyword ``global``.

Currying
--------

Una funzionalità utile è quella di avere una definizione di funzione che può
essere valutata parzialmente.

::

  f(x, y) = y /x
  g(y) = y / 2
  g(3) = 3 / 2

I linguaggi funzionali tradizionali supportano quest'approccio, ma con Python
basta usare una closure.

.. code-block:: python3

  def make_currying(f, a):
    def fc(\*args):
      return f(a, \*args)
    return fc

   def f2(x, y):
     return x + y

   a = make_currying(f2, 3)
   print a(1)

>>> python3 curry.py
4

Esiste ``partial`` in ``functools``.

Object Oriented Programming in Python
=====================================

Python, come già detto, è **object based**, nel senso che tutto quanto è un
oggetto.
Abbiamo il concetto di classe, c'è l'ereditarietà, ma l'istanza non è
instanziata dallo stesso template, bensì la classe è un prototipo, cioè si
stabilisce come sarà all'inizio, ma non le possibili estensioni.

OO filosofia di design
----------------------

I programmi sono un insieme di algoritmi e strutture dati. Nell'object oriented
programming l'applicazione è sempre dato-centrica.

I concetti fondamentali sono l'incapsulazione, il data hiding e l'astrazione.

Incapsulazione
  L'esposizione dell'interfaccia pubblica.

Data hiding
  Nascondere l'implementazione interna scelta, con ad esempio la possibilità di
  cambiarla.

Astrazione
  Fornire un template dei metodi e dell'interfaccia pubblica.
  
Il principio sarà *divide and conquer*, cioè dividere in segmenti più piccoli e
trattabili il problemi, scomporre.

Classi
------

.. code-block:: python3

  class Rectangle:

    def __init__(self, width, height):
      self.width = width
      self.height = height

    def area(self):
      return self.width * self.height

    def perimeter(self):
      return 2 * (self.width + self.height)

    def __str__(self):
      return "My sides are {0}, {1}.\nMy perimeter is {2}.\nMy area is {3}.\
             format(self.width, self.height, self.perimeter(), self.area())

>>> r = Rectangle(7, 42)
>>> print(r)
My sides are 7, 42.
My perimeter is 98.
My area is 294.

Inheritance
-----------

.. code-block:: python3

  class Shape:
    def area(self): pass
    def perimeter(self): pass
    def __str__(self): pass

.. code-block:: python3

  class Square(Rectangle):
    
    def __init__(self, side):
      super().__init__(side, side)

Duck typing
-----------

Gli oggetti se si comportano alla stessa maniera, cioè se hanno lo stesso
comportamento (e quindi ricevono gli stessi messaggi), allora tra di loro c'è
una relazione.

Non esiste il late binding, quindi l'ereditarietà ha solo una funzione di
convisione di condividere codice.

Variabili d'istanza e attributi di classe
-----------------------------------------

.. code-block:: python3

  class C:
    
    def __init__(self):
      self.attribute = 'a value'

    def __str__(self):
      return self.attribute

>>> from C import C
>>> c = C()
>>> c1 = X()
>>> c1.instance_attribute = 'prova'
>>> c.instance_attribute
AttributeError
>>> C.class_attribute = 42
>>> c1.class_attribute, c.class_attribute
(42, 42)

Tramite la chiamata a ``__dict__`` si può effettuare **introspezione**.

>>> c.__dict__
{'attribute': 'a value'}

Modifichiamo ``__str__`` in maniera da stampare tutti gli attributi.

.. TODO

Negli attributi della classe ci sono anche le funzioni e gli attibuti con gli
``__methods__``.

Quando creo un metodo nella classe è ancora una funzione, mentre se instanzio la
classe, il metodo diventa un bound method e quindi ``is`` non ritorna ``True``.

Descrittori
-----------

.. TODO

Il problema del diamante
------------------------

Rigurarda l'ereditarietà multipla. Come faccio a non eseguire due volte il nonno
quando il figlio esegue i due padri?

Utilizzo la **linearizzazione delle classi**, con delle liste ordinate di
ereditarietà.

``super()`` fa esattamente questo.

Iterators
---------

Un iteratore segue l'*iterator protocol*, cioè implementa i metodi ``__iter__``
e ``__next__``.

I generatori sono un uso speciale degli iteratori. Un esempio può essere
l'iterazione per la sequenza di Fibonacci.

Esempio dei plurali
###################

.. code-block:: python3

  class LazyRules:

    def __init__(self, rules_filename):
      self.pattern_file = open(rules_filename, encoding='utf-8')
      self.cache = []

    def __iter__(self):
      self.cache_index = 0
      return self

    def __next__(self):
      self.cache_index += 1
      if len(self.cache) >= self.cache_index:
        return self.cache[self.cache_index-1]
      if self.patter_file.closed:
        raise StopIteration
      line = self.patter_file.readline()
      if not line:
        self.pattern_file.close()
        raise StopIteration
      pattern, search, replace = line.split(None, 3)
      funcs = build_match_and_apply_functions(pattern, search, replace)
      self.cache.append(funcs)
      return funcs

  rulse = LazyRules()

Esempio: cryptarithms
>>>>>>>>>>>>>>>>>>>>>

::

  HAWAII + IDAHO + IOWA + OHIO == STATES
  510199 + 98153 + 9301 + 3593 == 621246

.. code-block:: python3

  import re, itertools, sys

  def solve(puzzle):
    words = re.findall('[A-Z]+', puzzle.upper())
    unique_characters = set(''.join(words))

    assert len(unique_characters) <= 10, 'Too many letters'

    first_letters = {word[0] for word in words}
    n = len(first_letters)
    sorted_characters = ''.join(first_letters) +
      ''.join(unique_characters-first_letters)
    characters = tuple(ord(c) for c in sorted_characters)
    digits = tuple(ord(c) for c in '0123456789')
    zero = digits[0]

    for guess in itertools.permutations(digits, len(characters)):
      if zero not in guess[:n]:
        equation = puzzle.translate(dict(zip(characters, guess)))
        if eval(equation):
          return equation

  if __name__ == '__main__':
    for puzzle in sys.argv[1:]:
      print(puzzle)
      solution = solve(puzzle)
      if solution:
        print(solution)

``itertools``
#############

Insieme di iteratori utili.

.. TODO: iterators implemented in slides

Test driven development
=======================

Esempio: romans
---------------

1. Funzione ß che trasforma numeri arabi in romani
2. Da 1 a 3999
3. Non c'è uno zero
4. Non ci sono numeri negativi
5. Esistono solo i numeri interi

Si devono creare delle funzioni dette unità di testing, che permettono di
verificare che le nostre funzioni facciano il loro lavoro.

Si usa il modulo ``unittest``.

Bisogna usare dei "casi noti", cioè dei test basati su comportamenti già noti,
perchè non posso testare anche l'algoritmo che userò per testare il codice.

.. code-block:: python3

  import unittest

  class Test(unittest.TestCase):

    def test_function(self):
      self.assertEqual(something1, something2)

  if __name__ == '__main__':
    unittest.main()

.. code-block:: python3
  
  # to_roman(n) / from_roman(n)
  ROMANS = (
    ('M', 1000),
    ('CM', 900),
    ('D', 500),
    ('CD', 400),
    ('C', 100),
    ('XC', 90),
    ('L', 50),
    ('XL', 40),
    ('X', 10),
    ('IX', 9),
    ('V', 5),
    ('IV', 4),
    ('I', 1)
  )

  class OutOfRangeError(ValueError): pass

  def to_roman(n):
    if not (0 < n < 4000):
      raise OutOfRangeError('Tra 1 e 3999')
    result = ''
    for rom, integ in ROMANS:
      while n >= integ:
        result += rom
        n -= integ
    return result

  def from_roman(r):
    result = 0
    index = 0
    for rom, integ in ROMANS:
      while s[index:index+len(rom)] == rom:
        result += integ
        index += len(rom)
    return result
